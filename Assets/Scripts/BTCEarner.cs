﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTCEarner : MonoBehaviour {
    public GameObject gameManager;


	// Use this for initialization
	void Awake () {
        gameManager = GameObject.Find("GameManager");
        gameManager.GetComponent<StatsManager>().BTCPerSecond += this.GetComponent<ObjectStats>().BTCGenerationRate;
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void removeHardware()
    {
        gameManager.GetComponent<StatsManager>().BTCPerSecond -= this.GetComponent<ObjectStats>().BTCGenerationRate;
    }
}
