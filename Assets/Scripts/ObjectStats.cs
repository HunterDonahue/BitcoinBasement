﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectStats : MonoBehaviour {

    //Use this method for public(private) variables and be able to get/set using the calls
    [SerializeField]
    public float t_BTCGenerationRate;
    [SerializeField]
    public int t_powerConsumption;
    [SerializeField]
    public float t_cost;

    public float BTCGenerationRate
    {
        get { return t_BTCGenerationRate; }
        set { t_BTCGenerationRate = value; }
    }

    public int powerConsumption
    {
        get { return t_powerConsumption; }
        set { t_powerConsumption = value; }
    }

    public float cost
    {
        get { return t_cost; }
        set { t_cost = value; }
    }


}

