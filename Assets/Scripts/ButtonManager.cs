﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {

    //Theres most likely a much better way to do this but im a lazy fuck
    public GameObject shopButton;
    public bool shopButtonActive;
    public GameObject shopButtonSubButtons;

    public GameObject networkingButton;
    public bool networkingButtonActive;
    public GameObject networkingButtonSubButtons;

    void Start()
    {

    }


    public void setActive(GameObject obj)
    {
        obj.SetActive(!obj.activeInHierarchy);
    }

    public void toggleButtonActive(string boolname)
    {
        switch (boolname)
        {
            case "shopButton":
                shopButtonActive = !shopButtonActive;
                setActive(shopButtonSubButtons);
                break;
            case "networkingButton":
                networkingButtonActive = !networkingButtonActive;
                setActive(networkingButtonSubButtons);
                break;
        }
    }

    public void toggleGamemodeActive(GameObject tog)
    {
        tog.SetActive(!tog.activeInHierarchy);
    }


    //This shit is annoying, lets no try and make a cool sliding effect right out of the bat

    /*
    public void openMenu(GameObject button, GameObject subButtonHost) {
        float xPos = button.transform.position.x + 80;
        float yPos = button.transform.position.y;
        foreach (Transform menu in subButtonHost.transform)
        {

            StartCoroutine(LerpTo(button, subButtonHost, 100, menu, xPos, yPos));
            xPos += 70;
        }
    }

    public void closeMenu(GameObject button, GameObject subButtonHost)
    {

    }

    public IEnumerator LerpTo(GameObject Button, GameObject subButtonHost, float time, Transform menu, float xPos, float yPos)
    {
        float elapsedTime = 0;
        while (elapsedTime < time)
        {
            menu.transform.position = Vector3.Lerp(shopButton.transform.position, new Vector3(xPos, yPos, 0), time * Time.deltaTime);
            elapsedTime += Time.deltaTime;
            yield return new WaitForSeconds(2);
        }
    }
    */
}
