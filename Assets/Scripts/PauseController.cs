﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {

    void OnEnable()
    {
        Debug.Log("Awake!");
        Time.timeScale = 0.0f;
    }

    void OnDisable()
    {
        Debug.Log("Disabled!");
        Time.timeScale = 1.0f;
    }
}
