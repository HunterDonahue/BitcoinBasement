﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetGameManager : MonoBehaviour {

    public GameObject gameManager;
    public ObjectPlacement objectPlacement;

    void Awake()
    {
        gameManager = GameObject.Find("GameManager");
        objectPlacement = gameManager.GetComponent<ObjectPlacement>();
    }

    public void closeUI()
    {
        objectPlacement.closeUI();
    }
}
